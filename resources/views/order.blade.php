@extends('layout')

@section('title', 'Підтверження замовлення')
@section('content')
    <h1>Підтверження замовлення:</h1>
    <div class="container">
        <div class="row justify-content-center">
            <p>Загальна сума замовлення: <b> {{$order->getFullPrice()}} грн.</b></p>
            <form action="{{route('order-confirm')}}" method="POST">
                <div>
                    <p>Вкажіть контактні дані, щоб   наш менеджер міг зв'язатись з вами:</p>

                    <div class="container">
                        <div class="form-group">
                            <label for="name" class="control-label col-lg-offset-3 col-lg-2">Имя: </label>
                            <div class="col-lg-4">
                                <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-offset-3 col-lg-2">Номер
                                телефону: </label>
                            <div class="col-lg-4">
                                <input type="text" name="phone" id="phone" value="" class="form-control">
                            </div>
                        </div>
                        <br>
                        <br>
{{--                        @guest--}}
                            <div class="form-group">
                                <label for="email" class="control-label col-lg-offset-3 col-lg-2">Email: </label>
                                <div class="col-lg-4">
                                    <input type="text" name="email" id="email" value="" class="form-control">
                                </div>
                            </div>
{{--                        @endguest--}}
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="сomment" class="control-label col-lg-offset-3 col-lg-2">Коментар до замовлення: </label>
                            <div class="col-lg-4">
                                <textarea name="сomment" id="сomment" value="" class="form-control"></textarea>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                    <br>
                    @csrf
                    <input type="submit" class="btn btn-success" value="Підтвердити замовлення">
                </div>
            </form>
        </div>
    </div>
@endsection