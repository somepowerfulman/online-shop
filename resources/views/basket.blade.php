@extends('layout')

@section('title', 'Корзина')
@section('content')
    <h1>Корзина</h1>
    <p>Оформление заказа</p>
    <div class="panel">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Товар</th>
                <th>Кількість</th>
                <th>Ціна</th>
                <th>ІТОГО</th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->products as $product)
                <tr>
                    <td>

                        <a href="{{ route('product_card', [$product->category->slug, $product->slug]) }}">
                            {{ $product->name }}
                            <img height="56px" src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}"></a>
                    </td>

                    <td>
                        <div class="btn-group form-inline">
                            <form action="{{route('rem_from_basket',[$product->id])}}" method="POST">
                                <button type="submit" class="btn btn-danger"
                                        href=""><span
                                            class="glyphicon glyphicon-minus" aria-hidden="true">-</span></button>
                                @csrf
                            </form>

{{--                            <span class=" badge badge-light">{{$product->pivot->count}}</span>--}}

                            <h5><span style="margin-top: -8px;" class="badge   badge-light">{{$product->pivot->count}}</span></h5>

                            <form action="{{route('add_in_basket',[$product->id])}}" method="POST">
                                <button type="submit" class="btn btn-success"
                                        href=""><span
                                            class="glyphicon glyphicon-plus" aria-hidden="true">+</span></button>
                                @csrf
                            </form>
                        </div>
                    </td>
                    <td>{{$product->price}} грн.</td>
                    <td>{{$product->getSummProduct($product->pivot->count)}} грн.</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="3">Загальна ціна:</td>
                <td>{{$order->getFullPrice()}}</td>
            </tr>

            </tbody>
        </table>
        <br>
        <div class="btn-group pull-right" role="group">
            @if($order->getFullPrice())

                <a type="button" class="btn btn-success" href="{{route('order')}}">Оформити замовлення</a>
            @else
                <a type="button" class="btn btn-success" href="{{route('index')}}">Вибрати товар</a>
            @endif
        </div>
        <div>
@endsection