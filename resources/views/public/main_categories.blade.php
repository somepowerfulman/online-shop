@extends('layout')
@section('title', 'Всі категорії')
@section('content')



  @foreach($categories as $category)
    <div class="starter-template">
        <div class="panel">
            <a href="/category/{{$category->slug}}">
                <img class="logo_img" src="{{Storage::url($category->image)}}">
                <h2>{{ $category->name}}</h2>
            </a>
            <p>
                {!!$category->description!!}
            </p>
        </div>

    </div>
    @endforeach

    @endsection
