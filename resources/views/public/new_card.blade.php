<head>
    <style>
        * {
            box-sizing: border-box;
        }

        .product-inner {
            width: 300px;
            margin: 20px auto;
            background: white;
            text-align: center;
            border-bottom: 2px solid #ebebec;
            transition: .2s linear;
        }

        .product-inner:hover {
            border-color: #5c6f02d1;
        }

        .product-wrap {
            position: relative;
            overflow: hidden;
            margin-bottom: 15px;
        }

        .product-img {
            max-height: 600px;
        }

        .product-wrap {
            height: 280px;
        }

        .product-wrap img {
            display: block;
            margin: 0 auto;
            max-width: 100%;
            /*text-align: center; !* Выравнивание по центру *!*/
            max-height: 280px;
        / /
        }

        .actions {
            position: absolute;
            /*left: 0;*/
            bottom: -20%;
            width: 100%;
            background: rgba(59, 62, 67, 0.75);
            transition: .3s linear;
        }

        .product-inner:hover .actions {
            bottom: 0;
        }

        .actions a {
            text-decoration: none;
            float: left;
            width: 50%;
            color: white;
            padding: 15px 0;
            transition: .2s linear;
        }

        .actions a:hover {
            background: rgba(59, 62, 67, 0.85);
        }

        .actions a:before {
            font-family: "FontAwesome";
        }

        /*.add-to-cart:before {content: "\f07a";}*/
        /*.quickview:before {content: "\f002";}*/
        /*.wishlist:before {content: "\f08a";}*/
        .product-info {
            padding-bottom: 10px;
            font-family: 'Noto Sans', sans-serif;
        }

        .product-title {
            margin: 0 0 10px 0;
            font-family: 'Noto Sans', sans-serif;
        }

        .product-title a {
            text-decoration: none;
            color: #1e1e1e;
            font-weight: 400;
            font-size: 16px;
        }

        .price {
            font-weight: bold;
            color: #5c6f02;
    </style>
</head>

<div class="col-sm-6 col-md-4">
    <div class="product-inner">
        @if($product->isNew())
        <span class="badge badge-info">Новинка</span>
        @endif
        @if($product->isHit())
        <span class="badge badge-warning">Хіт продаж</span>
        @endif
        <div class="product-wrap">
            <img src="{{Storage::url($product->image)}}">
            <div class="actions">
                <a href="{{route('add_in_basket',[$product->id])}}" class="add-to-cart">В корзину</a>
                <a href="{{route('product_card',[$product->category->slug, $product->slug])}}" class="quickview">Переглянути</a>
                <!-- <a href="" class="wishlist"></a> -->
            </div>
        </div>
        <div class="product-info">
            <h3 class="product-title"><a href="">{{$product->name}}</a></h3>
            <span class="price">{{number_format($product->price, 2,'.', ' ')}} грн.</span>
        </div>
    </div>
</div>
