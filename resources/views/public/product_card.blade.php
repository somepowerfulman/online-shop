@extends('layout')
@section('title', $products->name)
@section('content')


    <div class="starter-template">
        @if($products->isNew())
        <span class="badge badge-warning">Новинка</span>
        @endif
        @if($products->isHit())
        <span class="badge badge-success">Хіт продаж</span>
            @endif
        <h1>{{$products->name}}</h1>
        <h2>{{$products->category->name}}</h2>
        <p>Цiна: <b>{{$products->price}} грн.</b></p>
        <img class="product-img" src="{{Storage::url($products->image)}}">
        <p>{{$products->description}}</p>
        <form action="{{route('add_in_basket',[$products->id])}}" method="GET">
            Не доступен
            <input type="submit" class="btn btn-primary" role="button" value="В корзину" >
            {{--                <input type="hidden" name="_token" value="vEOGqGL5ZI7QBMRyraOGXobcNrIuZ3ezOuHGJX9p">--}}
            @csrf
        </form>
    </div>

@endsection