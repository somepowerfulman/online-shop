@extends('layout')
@section('title', 'Головна')
@section('content')
    <div class="starter-template">
        <h1>Всі товари</h1>
        <form method="GET" action="{{route('index')}}">
            <div class="filters row">
                <div class="col-sm-6 col-md-4">
                    <label for="price_from">Ціна від
                        <input type="text" name="price_from" id="price_from" size="5" value="{{request()->price_from}}">
                    </label>
                    <label for="price_to">до
                        <input type="text" name="price_to" id="price_to" size="6" value="{{request()->price_to}}">
                    </label>
                </div>
                <div class="col-sm-2 col-md-1">
                    <label for="hit">
                        <input type="checkbox" name="hit" id="hit" @if(request()->has('hit')) checked @endif> Хіт
                    </label>
                </div>
                <div class="col-sm-2 col-md-2">
                    <label for="new">
                        <input type="checkbox" name="new" id="new"  @if(request()->has('new')) checked @endif>
                        Новинка
                    </label>
                </div>
                <div class="col-sm-2 col-md-2">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="price" name="price" @if(request()->has('price')) checked @endif>
                        <label class="custom-control-label" for="price">Ціна</label>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <button type="submit" class="btn btn-primary">Фильтр</button>
                    <a href="{{route('index')}}" class="btn btn-warning">скинути</a>
                </div>
            </div>
        </form>

        <div class="row">
            @foreach($products as $product)
                @include("public.new_card")
            @endforeach
        </div>
        <ul class="pagination justify-content-center" style="margin:20px 0">
            <li class="page-item">{{$products->links()}}</li>
        </ul>


@endsection