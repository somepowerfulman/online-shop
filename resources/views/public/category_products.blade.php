@extends('layout')
@section('title', $category->name)
@section('content')



<div class="container">
    <div class="starter-template">
        <h1>
            {{$category->name}}
        </h1>
        <p>
            {!! $category->description!!}
        </p>
        <div class="row">
{{--            loop--}}
            @foreach($category->products as $product)

                @include('public.new_card')
            @endforeach
    </div>
</div>


@endsection