@extends('admin.home.home_layout')
@isset($category)
    @section('title', 'Редагувати категорію')
    @else
@section('title', 'Створити категорію')
        @endisset

@section('content')
    <div class="container" style="margin-top: 70px;">
        <form method="POST"  enctype="multipart/form-data"  id="main_input_box"
              @isset($category)
              action="{{route('categories.update', $category)}}">
            @else
                action="{{route('categories.store')}}" >
            @endisset
                @isset($category)
                    @method('PUT')
                @endisset
            @csrf
            <div class="form-group edit-form">

                <label for="prefix" class="control-label col-lg-offset-2 col-lg-2">Код(slug): </label>
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <input type="text" name="slug" id="prefix" value="{{old('slug', isset($category) ? $category->slug : null)}}" class="form-control">
                    </div>

               @error('slug')
                <div class="alert alert-danger col-lg-6 col-md-12">
                    {{$message}}
                </div>
                @enderror
                </div>
            </div>

            <div class="form-group edit-form">
                <label for="name" class="control-label col-lg-offset-2 col-lg-2">Назва: </label>
                <div class="row">
                <div class="col-lg-6 col-md-12">
                    <input type="text" name="name" id="name" value="{{old("name", isset($category) ? $category->name: null)}}" class="form-control">
                </div>
                    @error('name')
                    <div class="col-lg-6 col-md-12  alert alert-danger">
                        {{$message}}
                    </div>
                    @enderror
                </div>
            </div>


            <div class="form-group edit-form">
                <label class="control-label col-lg-offset-2 col-lg-2" for="description">Короткий опис:</label>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                <textarea name="description"  class="form-control" id="description" rows="3">{{old('description', isset($category) ? $category->description : null)}}</textarea>
            </div>
                @error('description')
                <div class="col-lg-6 col-md-12  alert alert-danger">
                    {{$message}}
                </div>
                @enderror
            </div>
            </div>

                <div class="form-group edit-form">
                    <label class="control-label col-lg-offset-2 col-lg-2">Select image for category</label>
                    <div class="edit-form col-lg-6">
                        <input  type="file" name="image" class="form-control-file">
                    </div>
                </div>
            <br><br>

            <div class="edit-form">
                <div class="col-lg-5"></div>
                <div class="col-lg-6">
                    <button  type="submit" class="btn btn-success"> Зберегти</button>
{{--                    <input  type="submit"  value=" Зберегти" class="btn btn-success">--}}
                </div>
            </div>
    </form>
    </div>
@endsection