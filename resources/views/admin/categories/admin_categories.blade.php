@extends('admin.home.home_layout')
@section('title', 'Категорії')
@section('content')
    <div class="container" style="margin-top: 70px;">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Ім'я</th>
                    <th>prefix</th>
                    <th>Іконка</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                <tr>

                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td>{{$category->slug}}</td>
                    <td><img style="max-height: 28px" src="{{\Illuminate\Support\Facades\Storage::url($category->image)}}" alt="image"></td>
                    <td>
                        <form action="{{route('categories.destroy',$category->id)}}" method="POST" id="main_input_box">
                        <a href="{{route('categories.show',$category->id)}}" class="btn btn-primary"> show</a>
                        <a href="{{route('categories.edit', $category->id)}}" class="btn btn-warning"> edit</a>
                            @csrf
                            @method('DELETE')
                        <button  class="btn btn-danger"> remove</button>
{{--                            <input type="submit" value="ok">--}}

                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        <ul class="pagination justify-content-center" style="margin:20px 0">
            <li class="page-item">{{$categories->links()}}</li>
        </ul>
            <a href="{{route('categories.create')}}" class="btn btn-success"> Додати категорію</a>

    </div>
@endsection