@extends('admin.home.home_layout')
@section('title', 'redact')
@section('content')
    <div class="container" style="margin-top: 70px;">
        <div class="col-lg-9" role ="form" id="main_input_box">
            <table class=" table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>parametr</th>

                </tr>
                </thead>
                <tbody>
                <tr>

                    <td>ID</td>
                    <td>{{$category->id}}</td>

                </tr>
                <tr>

                    <td>name</td>
                    <td>{{$category->name}}</td>

                </tr>

                <tr>

                    <td>Опис</td>
                    <td>{!!$category->description!!}</td>


                </tr>
                <tr>

                    <td>Одиниць товару в категорії</td>
                    <td>{{$category->products->count()}}</td>


                </tr>
                </tbody>
            </table>
        </div>

                    <div class="col-lg-3" id="img-div">
                        <img style="height: 128px" src="{{Storage::url($category->image)}}" alt="image">
{{--                        <img style="height: 128px" src="{{$category->image}}" alt="image">--}}

                    </div>
        <div class="col-lg-12">
        <a href="{{route('categories.index')}}" class="btn btn-default"> В список категорій</a>
        </div>
        </div>


@endsection