@extends('admin.home.home_layout')
@section('title', 'Товари')
@section('content')
    <div class="container" style="margin-top: 70px;">

            <table class="table table-hover">

                <thead>
                <tr>
                    <th>#</th>
                    <th style="max-width: 286px;">Назва</th>
                    <th >prefix</th>
                    <th >Категорія</th>
                    <th >Картинка</th>
                    <th >Ціна</th>
                    <th >Дії</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                <tr>

                    <td>{{$product->id}}</td>
                    <td style="max-width: 286px;">{{$product->name}}</td>
                    <td>{{$product->slug}}</td>
                    <td>{{$product->category->name}}</td>
                    <td><img style="max-height: 28px" src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}" alt="image"></td>
                    <td>{{number_format($product->price, 2, '.', ' ')}} грн.</td>
                    <td>
                        <form action="{{route('products.destroy', $product->id)}}" method="POST" id="main_input_box">
                            @method('DELETE')
                            @csrf
                        <a href="{{route('products.show',$product->id)}}" class="btn btn-primary"> show</a>
                        <a href="{{route('products.edit', $product->id)}}" class="btn btn-warning"> edit</a>
                        <button  class="btn btn-danger"> remove</button>

                           </form>
                        </td>
                </tr>
                @endforeach

                </tbody>

            </table>
        <ul class="pagination justify-content-center" style="margin:20px 0">
            <li class="page-item">{{$products->links()}}</li>
        </ul>
            <a href="{{route('products.create')}}" class="btn btn-success"> Додати товар</a>

    </div>
@endsection