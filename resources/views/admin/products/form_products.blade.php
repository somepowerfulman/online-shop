@extends('admin.home.home_layout')
@isset($product)
    @section('title', 'Редагувати продукт')
@else
    @section('title', 'Створити продукт')
@endisset
@section('content')
    {{--    {{dd($categories)}}--}}
    <div class="container" style="margin-top: 70px;">
        <form enctype="multipart/form-data" method="POST" id="main_input_box"
              @isset($product)
              action="{{route('products.update',$product)}}">
            @method('PUT')
            @else

                action="{{route('products.store')}}">
            @endisset
            @csrf


            <div class="form-group edit-form">
                <label for="categotry_id" class="control-label col-lg-offset-2 col-lg-2"> Select category</label>
                <div class="row">
                    <div class="col-lg-6">

                        <select name="categotry_id" class="custom-select " id="inlineFormCustomSelectPref">

                            @foreach($categories as $category)
                                {{--                                <option value="{{old('categotry_id', isset($category->id) ? $category->name :null)}}">{{$category->name}} </option>--}}
                                <option value="{{$category->id}}">{{$category->name}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>

            <div class="form-group edit-form">
                <label for="prefix" class="control-label col-lg-offset-2 col-lg-2">Код(slug): </label>
                <div class="row">
                    <div class="col-lg-6">
                        <input type="text" name="slug" id="prefix"
                               value="{{old('slug', isset($product->slug) ? $product->slug:null )}}"
                               class="form-control">
                    </div>
                 @include('admin.errors', ['fieldName'=>'slug'])
                </div>
            </div>


            <div class="form-group edit-form">
                <label for="name" class="control-label col-lg-offset-2 col-lg-2">Назва: </label>
                <div class="row">
                    <div class="col-lg-6">
                        <input type="text" name="name" id="name"
                               value="{{old('name',isset($product->name)? $product->name : null )}}"
                               class="form-control">
                    </div>
                    @include('admin.errors', ['fieldName'=>'name'])
                </div>

                <div class="form-group edit-form">
                    <label for="price" class="control-label col-lg-offset-2 col-lg-2">Price: </label>
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="price" name="price" id="price"
                                   value="{{old('price', isset ($product) ? $product->price : null)}}"
                                   class="form-control">
                        </div>
                        @include('admin.errors', ['fieldName'=>'price'])
                    </div>
                </div>
                <div class="form-group edit-form">
                    <label class="control-label col-lg-offset-2 col-lg-2" for="description">Короткий опис:</label>
                    <div class="row">
                        <div class="col-lg-6">
                            <textarea name="description" class="form-control" id="description"
                                      rows="3">{{old('description')}}@isset($product->description){{$product->description}}@endisset</textarea>
                        </div>
                        @include('admin.errors', ['fieldName'=>'description'])
                    </div>
                </div>
                <div class="form-group edit-form">
                    <label class="control-label col-lg-offset-2 col-lg-2" for="image">Select image for
                        product</label>
                    <div class="edit-form col-lg-6">
                        <input type="file" name="image" class="form-control-file">
                    </div>
                </div>
                <div class="filters row">

                        @foreach(['hit'=>'Хіт',
                        'new'=>'Новинка'
                         ] as $field=>$title)
                        <div class="col-sm-2 col-md-2">
                        <label for="{{$field}}">
                                <input type="checkbox" name="{{$field}}" id="" @if(isset($product)&&$product->$field===1) checked="checked" @endif> {{$title}}
                            </label>
                    </div>
                    @endforeach




                </div>

                <div class="edit-form">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success"> Зберегти</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection