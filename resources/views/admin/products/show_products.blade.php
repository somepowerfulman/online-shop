@extends('admin.home.home_layout')
@section('title', 'redact')
@section('content')
    <div class="container" style="margin-top: 70px;">
        <div class="col-lg-9" role ="form" id="main_input_box">
            <table class=" table table-hover">
                <thead>
                <tr>

                    <th>#</th>
                    <th>parametr</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <div class="col-lg-3 text-center" id="img-div">
                        @if($product->isNew())
                            <span class="badge badge-warning">Новинка</span>
                        @endif
                        @if($product->isHit())
                            <span class="badge badge-success">Хіт продаж</span>
                        @endif
                        <img style="height: 256px" src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}" alt="image">

                    </div>
                    <td>ID</td>
                    <td>{{$product->id}}</td>

                </tr>
                <tr>

                    <td>Категорія</td>
                    <td>{{$product->category->name}}</td>

                </tr>
                <tr>

                    <td>Найменування</td>
                    <td>{{$product->name}}</td>

                </tr>

                <tr>

                    <td>Опис</td>
                    <td>{!!nl2br($product->description)!!}</td>


                </tr>
                <tr>

                    <td>Ціна</td>
                    <td>{{number_format($product->price, 2,'.',' ')}} грн.</td>


                </tr>
                </tbody>
            </table>
        </div>


        <div class="col-lg-12">
        <a href="{{route('products.index')}}" class="btn btn-success"> В список products</a>
        </div>
        </div>


@endsection