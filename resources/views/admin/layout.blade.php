
<html lang="en" class="mdl-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    {{--    <link href="/css/bootstrap.min.css" rel="stylesheet">--}}
    {{--    <link href="/css/starter-template.css" rel="stylesheet">--}}
    {{--    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>--}}
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>





{{--    <link href="/css/bootstrap.min.css" rel="stylesheet">--}}
{{--    <link href="/css/starter-template.css" rel="stylesheet">--}}
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Інтернет магазин</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Всі товари</a></li>
                <li><a href="{{route('categories_all')}}">Категорії</a>-
                <li><a href="{{route('basket')}}">Кошик</a></li>
                  </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="./">Default <span class="sr-only">(current)</span></a></li>
                <li><a href="../navbar-static-top/">Static top</a></li>
                <li><a href="../navbar-fixed-top/">Fixed top</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>

{{-- <nav class="navbar navbar-inverse navbar-fixed-top">--}}
{{--        <div class="container">--}}
{{--            <div class="navbar-header">--}}
{{--                <a class="navbar-brand" href="/">Интернет Магазин</a>--}}
{{--            </div>--}}
{{--            <div id="navbar" class="collapse navbar-collapse">--}}
{{--                <ul class="nav navbar-nav">--}}
{{--                    <li class="active"><a href="/">Все товары</a></li>--}}
{{--                    <li><a href="/categories">Категории</a>--}}
{{--                    </li>--}}
{{--                    <li><a href="{{route('basket')}}">В корзину</a></li>--}}
{{--                    <li><a href="/">Сбросить проект в начальное состояние</a></li>--}}
{{--                </ul>--}}

{{--                <ul class="nav navbar-nav navbar-right">--}}
{{--                    <li><a href="http://internet-shop.tmweb.ru/login">Войти</a></li>--}}

{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </nav>--}}
<div class="container">

    @yield('content')

</div>
</body></html>





