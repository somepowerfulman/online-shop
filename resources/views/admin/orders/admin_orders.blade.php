@extends('admin.home.home_layout')
@section('title', 'Замовлення')

@section('content')
    <div class="container" style="margin-top: 70px;">
      <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Ім'я</th>
                    <th>USER</th>
                    <th>Коментар</th>
                    <th>дата</th>
                    <th>сума</th>
                    <th>Дії</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
{{--                    @section('user', $order->name)--}}
@checkadmin
<form action="{{ route('order.show',$order->id)}}" method="GET" role ="form"  id="main_input_box">
@endcheckadmin
                   <form action="{{ route('customer.show',$order->id)}}" method="GET" role ="form"  id="main_input_box">
                    <tr>

                    <td>{{$order->id}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->comment}}</td>
                    <td>{{$order->created_at->format('H:i d-m-Y')}}</td>
                    <td> {{$order->getFullPrice()}}</td>
                    <td>

                        <input type="submit"  value="open" class="btn btn-warning">
                    </td>
                </tr>
                    </form>

                @endforeach
                </tbody>
            </table>
        <ul class="pagination justify-content-center" style="margin:20px 0">
            <li class="page-item">{{$orders->links()}}</li>
        </ul>

    </div>
@endsection