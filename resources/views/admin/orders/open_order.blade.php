@extends('admin.home.home_layout')
@section('title', 'Замовлення')

@section('content')
    <div class="container" style="margin-top: 70px;">
        <h1>Замовлення - {{$order->id}} </h1>
        <p>Замовник {{$order->name}}</p>
        <p>Телефон: {{$order->phone}}</p>
        <p>e-mail: {{$order->email}}</p>
        <p>Коментар: {{$order->comment}}</p>
        <div class="panel">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Категорія</th>
                    <th>name</th>
                    <th>img</th>
                    <th>count</th>
                    <th>Ціна</th>

                </tr>
                </thead>

                <tbody>
{{--                {{dd($order)}}--}}
                @foreach($order->products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->category->name}}</td>
                        <td>
                            <a href="{{route('product_card',$product->category->slug.'/'.$product->slug)}}">
                                <img height="56px" src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}"></a>
                        </td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->pivot->count}}</td>
                        <td>{{$product->price}}</td>

                    </tr>
                @endforeach


{{--                        <td>--}}
{{--                            <div class="btn-group form-inline">--}}
{{--                                <form action="/" method="POST">--}}
{{--                                    <button type="submit" class="btn btn-danger"--}}
{{--                                            href=""><span--}}
{{--                                                class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>--}}
{{--                                    @csrf--}}
{{--                                </form>--}}

{{--                                <span class="badge"></span>--}}

{{--                                <form action="" method="POST">--}}
{{--                                    <button type="submit" class="btn btn-success"--}}
{{--                                            href=""><span--}}
{{--                                                class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>--}}
{{--                                    @csrf--}}
{{--                                </form>--}}

{{--                        </td>--}}
{{--                        <td>{{order_id}}</td>--}}



                <br>

                <tr >
                     <td class="border-top border-danger" colspan="3">Phone</td>
                    <td class="border-top border-danger" colspan="3">{{$order->phone}}</td>
                </tr>
                <tr>
                    <td colspan="3">Comment</td>
                    <td colspan="3">{{$order->comment}}</td>
                </tr>
                <tr>
                    <td colspan="3">Загальна ціна:</td>
{{--                    {{dd($order[0])}}--}}
                    <td colspan="3">{{$order->getFullPrice()}}</td>

                </tr>

                </tbody>
            </table>

{{--    </form>--}}
    </div>
        <br>
        <div class="btn-group pull-right" role="group">
{{--            @if($order->getFullPrice())--}}

{{--                <a type="button" class="btn btn-success" href="{{route('order')}}">Оформити замовлення</a>--}}
{{--            @else--}}
{{--                <a type="button" class="btn btn-success" href="{{route('index')}}">Вибрати товар</a>--}}
{{--            @endif--}}
        </div>
        <div>
@endsection