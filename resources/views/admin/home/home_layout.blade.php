<html lang="en" class="mdl-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link href="/css/starter-template.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="/">Інтернет магазин</a>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li  @if(Route::currentRouteNamed('customer.index')) class="active border-bottom" @endif>
                <a  class="nav-link " href="{{route('customer.index')}}">Мої замовлення <span class="sr-only">(current)</span></a>
            </li>
            @checkadmin
            <li  @if(Route::currentRouteNamed('home')) class="active border-bottom" @endif>
                <a  class="nav-link " href="{{route('home')}}">Всі замовлення <span class="sr-only">(current)</span></a>
            </li>
            <li  @if(Route::currentRouteNamed('categories.index')) class="active border-bottom" @endif>
                <a class="nav-link" href="{{route('categories.index')}}">Категорії</a>
            </li>
            <li  @if(Route::currentRouteNamed('products.index')) class="active border-bottom" @endif>
                <a class="nav-link" href="{{route('products.index')}}">Товари</a>
            </li>
            @endcheckadmin
        </ul>
        {{--        <form class="form-inline my-2 my-lg-0">--}}
        {{--            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">--}}
        {{--            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
        {{--            <div id="navbar" class="navbar-collapse collapse">--}}

        <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown nav-item" style="min-width: 161px" >
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@checkadmin Admin - @endcheckadmin {{\Illuminate\Support\Facades\Auth::user()->name}}<span class="caret"></span></a>

                        <ul class="dropdown-menu">
                            @checkadmin
                            <li><a  class="nav-link" href="{{route('home')}}">Особистий кабінет</a></li>
                            @else
                            <li><a  class="nav-link" href="{{route('home')}}">Мої замовлення</a></li>
                           @endcheckadmin
                            <li><a  class="nav-link" href="{{route('logout')}}">Вийти</a></li>
                        </ul>
                    </li>
                </ul>

    </div>
    </div>
    {{--        </form>--}}
    {{--    </div>--}}
</nav>

<div class="container">


    @yield('content')

</div>

</body></html>





