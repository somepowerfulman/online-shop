{{--   Open layout--}}
    <html lang="en" class="mdl-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

{{--        <link href="/css/bootstrap.min.css" rel="stylesheet">--}}
{{--        <link href="/css/new-product.css" rel="stylesheet">--}}
        <link href="/css/starter-template.css" rel="stylesheet">
{{--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--}}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>



    </head>
    <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="/">Інтернет магазин</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li  @if(Route::currentRouteNamed('index')) class="active border-bottom" @endif>
                        <a  class="nav-link " href="{{route('index')}}">Головна <span class="sr-only">(current)</span></a>
                    </li>
                    <li  @if(Route::currentRouteNamed('categor*')) class="active border-bottom" @endif>
                        <a class="nav-link" href="{{route('categories_all')}}">Категорії</a>
                    </li>
                    <li  @if(Route::currentRouteNamed('basket')) class="active border-bottom" @endif>
                        <a class="nav-link" href="{{route('basket')}}">Кошик</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @guest
                        <li  class=" border-bottom"><a class="nav-link " href="{{route('login')}}">Вхід</a></li>

                    @endguest
                        @auth
                    <li class="dropdown nav-item" style="min-width: 161px" >
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User <span class="caret"></span></a>

                        <ul class="dropdown-menu">
                            <li><a  class="nav-link" href="{{route('home')}}">Особистий кабінет</a></li>
                            <li><a  class="nav-link" href="{{route('logout')}}">Вийти</a></li>
                        </ul>
                    </li>
                        @endauth
                </ul>

            </div>
        </div>
    </nav>

    <div class="container">
        <div class="starter-template">
            @if(session()->has('success'))
                <p class="alert alert-success">{{session()->get('success')}}</p>
            @elseif(session()->has('error'))
                <p class="alert alert-danger">{{session()->get('error')}}</p>
            @elseif(session()->has('danger'))
                <p class="alert alert-danger">{{session()->get('danger')}}</p>
            @endif
        </div>

@yield('content')

    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    </body></html>





