@extends('auth.auth-layout')

@section('form')

    <p style="color: #39ace7; font-size: large">Вхід</p>
    <!-- Login Form -->
    <form method="POST" action="{{route('login')}}">
        @csrf
        <input type="email" id="email" class="fadeIn second" name="email" placeholder="email">
        <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
        <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
        <a class="underlineHover" href="{{route('register')}}">Реєстрація</a>
    </div>
@endsection
