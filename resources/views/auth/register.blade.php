@extends('auth.auth-layout')

@section('form')



        <p style="color: #39ace7; font-size: large">Реєстрація</p>
        <!-- Login Form -->
        <form  method="POST" action="{{ route('register') }}">
            @csrf
            <input type="email" id="email" class="fadeIn third" name="email" placeholder="email">
            <input type="text" id="name" class="fadeIn second" name="name" placeholder="name">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
            <input id="password-confirm" type="password" class="fadeIn third" name="password_confirmation" placeholder="confirm password">

            <input type="submit" class="fadeIn fourth" value="Реєстрація">

        </form>

        <div id="formFooter">
            <a class="underlineHover" href="{{route('login')}}">Вхід</a>
        </div>
@endsection
