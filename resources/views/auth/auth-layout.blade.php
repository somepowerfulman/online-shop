<html lang="en" class="mdl-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <link href="/css/form/login-form.css" rel="stylesheet"></link>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>

    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                  <!-- Icon -->
                <div class="fadeIn first">

                    <a href="{{route('index')}}"><img style="max-height: 184px; padding: 16px" src="/svg/shop.svg"  id="icon" alt="Інтернет магазин" /></a>
                </div>
                @yield('form')
            </div>
        </div>
    </body>
</html>