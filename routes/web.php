<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
//home
//Route::group(
//    ['middleware'=>'auth']

Route::get("/logout", "Auth\LoginController@logout")->name("get-logout");
Route::middleware('auth')->group(function () {

    Route::group([
        'prefix' => 'customer'
    ], function () {
        Route::get("/orders/{id}", "Customer\OrderController@show")->name("customer.show");
        Route::get("/orders", "Customer\OrderController@index")->name("customer.index");
    });

    Route::group([
        'middleware' => 'is_admin',
        'prefix' => 'admin'], function () {
        Route::get("/orders/{id}", "Admin\OrdersController@show")->name("order.show");
        Route::get("/orders", "Admin\OrdersController@index")->name("home");
        Route::resource("/categories", "Admin\CategoryController");
        Route::resource('/products', 'Admin\ProductController');
    });


});

//open
Route::post('/test', 'BasketController@test')->name('test');
Route::get("/", "TestController@index")->name("index");
Route::get("/category/{category}", 'TestController@category')->name('categories');
Route::get("/categories", 'TestController@categories_all')->name('categories_all');;
Route::get('/card/{category}/{product?}', 'TestController@products_card')->name('product_card');

//basket routes
Route::get('/order', 'BasketController@order')->name('order')->middleware('basket_not_empty')->middleware('auth');
Route::post('/order/confirm', 'BasketController@orderConfirm')->name('order-confirm');
Route::get('basket/add/{id?}', 'BasketController@addInBasket')->name('add_get_basket');
Route::group([
    'middleware' => 'basket_not_empty',
    'prefix' => 'basket'
], function () {
    Route::get('/', 'BasketController@basket')->name('basket');
//add-remove product in basket
    Route::post('/add/{id?}', 'BasketController@addInBasket')->name('add_in_basket');
    Route::post('/remove/{id?}', 'BasketController@removeFromBasket')->name('rem_from_basket');
});




