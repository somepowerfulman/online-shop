<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    public function basket()
    {
        $sessionOrderId = session('orderId');
        if ($sessionOrderId != null)
        {
            $order = Order::find($sessionOrderId);
            return view("basket", compact('order'));
        }
        session()->flash('danger',"basket empty");
        return redirect()->back();
    }

    public function order()
    {
        $sessionOrderId = session('orderId');
        if (is_null($sessionOrderId))
        {
            return redirect()->route('index');
        }
        $order = Order::find($sessionOrderId);
        if (Auth::check()){
//        dd($OrderId->user_id=Auth::user());
            $order->user_id=Auth::id();
            $order->save();
        }
        return view("order", compact('order'));
    }




    public function orderConfirm(Request $request)
    {
        $sessionOrderId=session('orderId');
        if (is_null($sessionOrderId)){
            session()->flash('danger',"orderConfirm problem session empty");
            return redirect()->route('index');
        }
        $order=Order::find($sessionOrderId);
//        dd($order);
        $recorded=$order->saveOrder($_POST['name'], $request->email, $request->phone, $_POST['сomment']);
        $request->session()->forget('orderId');
//        dd($recorded);

        //
//       додати юзера
        if ($recorded){
            session()->flash('success','Ваше замовлення прийнято до виконання');

        }else{
            session()->flash('error','Вибачте, відбулася помилка, спробуйте оформити замовлення ще раз');
        }

        return redirect()->route('index');

    }

    public function addInBasket($id){

        $sessionOrderId=session('orderId');
        $product=Product::find($id);
//        dump($sessionOrderId);
    if (is_null($sessionOrderId)){
        $order=Order::create();
        session(['orderId'=> $order->id]);
    }else{
        $order=Order::find($sessionOrderId);                //find передаєм ІД сесії повертає обєкт рядка БД
    }


    if ($order->products->contains($id))
    {
        $pivot_row=$order->products()->where('product_id', $id)->first()->pivot;
        $pivot_row->count++;
        $pivot_row->update();
//        dump($pivot_row);

        session()->flash('success', "Додано ще один  ".$product->name);
        return redirect()->route('basket');
    }else{
        $order->products()->attach($id);
        session()->flash('success', $product->name." додано до кошика");

        return redirect()->route('basket');
    }


                                                    //dump($order->products());      // отримую таблицю звязків"order_product"
//        $order->products()->attach($id);              //записую в таблицю order_product" - ІД заказу з запроса
                                                //        return view("basket", compact('order'));
//
    }
    public function removeFromBasket($id){
        $sessionOrderId=session('orderId');
        $product=Product::find($id);

//            if (is_null($sessionOrderId)){
////                return redirect()->route('basket');
//                dump('pusto');
            //}
        $order=Order::find($sessionOrderId);
            if ($order->products->contains($id)){
                $pivot=$order->products()->where('product_id', $id)->first()->pivot;
                if (($pivot->count)>1){
                    $pivot->count--;
                    $pivot->update();

                }else{
                    $order->products()->detach($id);

                }
            }


        session()->flash('danger',$product->name." видалено з кошика");
        return redirect()->route('basket');
    }

    public function test(Request $request){
        dump($request);
    }
}
