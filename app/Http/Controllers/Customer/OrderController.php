<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(){
//dd(Auth::user()->getOrders()->get());
//        $orders=Auth::user()->getOrders()->where('status',1)->get();
        $orders=Auth::user()->getOrders()->where('status',1)->paginate(10);
        return view('admin.orders.admin_orders', compact('orders'));
    }

    public function show($order){
//        dd(Auth::user());
//        dd($order);
        $order=Order::where('id',$order)->first();
        return view('admin.orders.open_order', compact('order'));
    }
}
