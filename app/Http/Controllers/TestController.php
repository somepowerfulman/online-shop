<?php

    namespace App\Http\Controllers;

    use App\Category;
    use App\Http\Requests\ProductsFilrerRequest;
    use App\Product;
    use Illuminate\Http\Request;

    class TestController extends Controller
    {
        public function index(ProductsFilrerRequest $request){
                  $productQuerry=Product::query();
//                    dd($request->filled());
            if ($request->filled('price_from'))
            {
                $productQuerry->where('price','>=', $request->price_from);
            }
            if ($request->filled('price_to'))
            {
                $productQuerry->where('price','<=', $request->price_to);
            }
            if ($request->filled('hit'))
            {
                $productQuerry->where('hit',1);
            }
            if ($request->filled('new'))
            {
                $productQuerry->where('new',1);
            }
            if ($request->filled('price'))
            {
                $productQuerry->orderBy('price');
            }
            $products=$productQuerry->paginate(6)->withPath("?".$request->getQueryString());
//            $products=Product::get();
            return view('public.index', compact('products'));
        }

        public function category($code)
        {
            //model Category
    //        model Product
            $category = Category::where('slug', $code)->first();
    //        $products=Product::where('categotry_id',$category->id)->get();
    //        dump($products);
            return view('public.category_products', compact('category'));
        }

        public function categories_all(){
            $categories=Category::get();
    //        dump($categories);
            return view('public.main_categories', compact('categories'));

        }

        public function products_card($category, $product=null){
//       $products=Product::get();
       $products=Product::where('slug',$product)->first();
    //    dump($category, $products);
    //    dump(['product'=>$products]);
        return view('public.product_card', compact('products'));
//        return view('public.product_card', ['product'=>$products]);

        }


    }
