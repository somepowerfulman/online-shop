<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;

class OrdersController extends Controller
{
    public function index(){
        $orders=Order::where('status', 1)->paginate(10);
//        dd($orders);
    return view('admin.orders.admin_orders', compact('orders'));
}
//public function show(Request $order){
////        $order=Order::where('status', 1)->get();
////    $order
//        dd($order);
//        return view('admin.orders.open_order', compact('order'));
//}
    public function show($order){
        $order=Order::where('id',$order)->first();
        return view('admin.orders.open_order', compact('order'));
    }
}
