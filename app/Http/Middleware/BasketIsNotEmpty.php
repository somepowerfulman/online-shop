<?php

namespace App\Http\Middleware;

use App\Order;
use Closure;

class BasketIsNotEmpty
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sessionOrderId = session('orderId');
        if (!is_null($sessionOrderId))
        {
            $order = Order::findOrFail($sessionOrderId);
            if ($order->products->count() == 0)
            {
                session()->flash('error', 'Корзина пуста ');
                //  return back(); //redirect back
                return redirect()->route('index');
            }
            elseif ($order->products->count() > 0)
            {
                return $next($request);
            }
        }
        else{
            session()->flash('error', 'Корзина пуста');
        }
        return $next($request);
    }
}
