<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $my_rools=[
            'slug'=>'required|min:3|max:255|unique:categories,slug',
            'name'=>'required|min:3|max:255',
            'description'=>'required|min:5|max:255'
        ];
        if ($this->route()->named('categories.update')){
            $my_rools['slug'].=','.$this->route()->parameter('category')->id;
        }
//        dd($my_rools);
        return $my_rools;
    }
    public function messages()
    {
        return [
            'required'=>"Поле :attribute обов'язкове",
            'min'=>'в полі :attribute має бути мінімум :min знаків',
            'max'=>'в полі :attribute має бути масимум :max знаків',
            'unique'=>'Даннний код вже існує, спробуйте інший'
        ];
    }
}
