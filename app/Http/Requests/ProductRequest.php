<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $my_rools=[
            'slug'=>'required|min:3|max:255|unique :products,slug',                 //unique table'products' -> field 'slug'
            'name'=>'required|min:3|max:255',
            'description'=>'required|min:8',
            'price'=>'required|numeric|min:1'
        ];
        if ($this->route()->named('products.update')){
//            'email' => 'unique:users,email_address,10'

            $my_rools['slug'].= ','.$this->route()->parameter('product')->id;    //parameter('product') - parametr rquest
        }
//        dd(get_class_methods($this->route()));
//        dd($my_rools);
        return $my_rools;
    }
    public function messages()
    {



        return [
            'required'=>"Поле :attribute обов'язкове",
            'min'=>'в полі :attribute має бути мінімум :min знаків',
            'max'=>'в полі :attribute має бути масимум :max знаків',
            'unique'=>'Даннний код вже існує, спробуйте інший'
        ];
    }
}
