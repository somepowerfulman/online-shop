<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=['id','name','slug', 'image', 'description'];
    //
    public function  product(){
        return $this->hasOne(Category::class);
    }
    public function products(){
//        return $this->hasMany('App\Product', "categotry_id", 'id' );
        return $this->hasMany(Product::class, "categotry_id", 'id' );
    }
}
