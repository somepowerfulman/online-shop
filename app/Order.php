<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
   public function products(){
       return $this->belongsToMany(Product::class)->withPivot('count')->withTimestamps();
   }
//   public function getUser(){
//       return $this->belongsTo(User::class);
//   }

public function getFullPrice(){
   $summ=0;
    foreach ($this->products as $item) {
        $summ+=$item->getSummProduct();
   }
   return $summ;
   }

   public function saveOrder($name,$email,$phone,$comment){
       if ($this->status==0) {
           $this->status = 1;
           $this->name=$name;
           $this->email=$email;
           $this->phone=$phone;
           $this->comment=$comment;
           $this->save();
           return true;
       }
       else{
           return false;
       }
   }
}
