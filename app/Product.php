<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['id', 'name', 'categotry_id', 'slug', 'price', 'description', 'image', 'hit', 'new'];
//   public function getCategory(){
//       $collection= Category::find($this->categotry_id);
//      //       $collection=Category::where('id', $this->categotry_id)->first();//variant 2
//      // якщо умова(типу where) з лівої сторони то це кусок запроса, якщо правої сторони -це робота з колекцією
//
//      //       $collection=Category::where('id', $this->categotry_id)->get(); return query
//      //       $collection=Category::where('id', $this->categotry_id);//query build
//
//      return $collection;
//   }

// public function category(){
//     return $this->belongsTo(Product::class, );
// }
    public function category()
    {
//     return $this->belongsTo('App\Category', "categotry_id", 'id' );
        return $this->belongsTo(Category::class, "categotry_id", 'id');
    }

    public function getSummProduct()
    {
        return $this->price * $this->pivot->count;

    }
    public function setNewAttribute($value)
    {
        $this->attributes['new'] = $value === 'on' ? 1 : 0;
    }
    public function setHitAttribute($value)
    {
        $this->attributes['hit'] = $value === 'on' ? 1 : 0;
    }
    public function isNew()
    {
        return $this->new===1;
    }

    public function isHit()
    {
        return $this->hit===1;
    }

}
